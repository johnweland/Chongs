var debugMode = false;

//
//  collapsable toggle for header in mobile view header.
//
$('#toggle').click(function() {
  $(this).next('.nav').toggleClass("is-collapsed-mobile");
});

//
//  Scrolling modified header.
//    
function init() {
    window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 150,
            header = document.querySelector("header");
        if (distanceY > shrinkOn) {
            $(header).addClass('smaller');
        } else {
            if ($(header).hasClass('smaller')) {
                $(header).removeClass('smaller');
            }
        }
    });
}
window.onload = init();

//
//	JavaScript for progressively enhanced and gracefully degrdated maps.
//
if ($('#locations').length) {
  $('#locations h1').after('<div id="map" class="col-sm-12"></div>');
  $('.static__map').remove();

  var map;
  var bounds;
  var geocoder;
  var center;
  function initialize() {
      var mapOptions = {
        center: { lat: -34.397, lng: 150.644},
        zoom: 8
      };
      map = new google.maps.Map(document.getElementById('map'),
          mapOptions);
      geocoder = new google.maps.Geocoder();
      bounds = new google.maps.LatLngBounds();
    }

    function addMarkerToMap (location, address) {
    	var image = "assets/img/foodbowl.gif";
    	var marker = new google.maps.Marker({map: map, position: location, icon: image});
    	bounds.extend(location);
    	map.fitBounds(bounds);
    	var infoWindow = new google.maps.InfoWindow({content : address})
    	google.maps.event.addListener(marker, "click", function(){
  		infoWindow.open(map, marker);
    	});
    }

    initialize();
    $('address').each( function(){
    	var $address = $(this);
  	geocoder.geocode({address: $address.text()}, function(results, status){
    		if(status == google.maps.GeocoderStatus.OK) addMarkerToMap (results[0].geometry.location, $address.html());
    	})
    });
    google.maps.event.addDomListener(map, "idle", function (){
    	center = map.getCenter();
    });

    $(window).resize(function(){
    	map.setCenter(center);
    });
} else {
  if ( debugMode == true ) {
    alert('no Locations');
  }
}
//
//  JavaScript Flicker Feed.
//

// jFlickerFeed setup.
if ($('#mygallery').length) {
  $('#mygallery').jflickrfeed({
    limit: 15,
    qstrings:{
        id: '127906695@N06'
    },
    itemTemplate: 
        '<a href="{{image_b}}">' +
            '<img src="{{image_m}}" alt="{{title}}" />' +
        '</a>'
}, function () {
    $("#mygallery").justifiedGallery({
        rowHeight : 100,
        maxRowHeight :100,
        lastRow : 'justify',
        margins : 3,
        randomize : true
    });
});
} else {
  if ( debugMode == true ) {
    alert('no Gallery');
  }  
}